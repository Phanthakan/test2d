package test3d;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
public class test3d extends JPanel implements ActionListener
{
	Timer tm = new Timer(5, this);
	int x = 0,velX = 2;
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(30,x,50,30);
		tm.start();
	}
	public void actionPerformed(ActionEvent e)
	{
		if(x < 0 || x > 550)
			velX = -velX;
		x = x + velX;
		repaint();
	}
	public static void main(String[] args) 
	{
		test3d t = new test3d();
		JFrame jf = new JFrame();
		jf.setTitle("test3d");
		jf.setSize(600, 400);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(t);
	}

}
